<?php
namespace TADS\App;

use Kosan\Framework\Module\MetaBox;
use Carbon_Fields\Field;

defined('ABSPATH') or die('Direct access not allowed');
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

class Page extends MetaBox
{
    protected $name = "Pengaturan Kloking";
    protected $post_type = "page";
    public $content;
    
    public function view()
    {
		$q = new \WP_Query( [
			'post_type' => 'page',
			'posts_per_page' => 1000,
		] );
		$pgs = $q->posts;
		$pages = [
			'' => 'Pilih Halaman'
		];
		foreach($pgs as $pg)
		if($pg->ID != @$_GET['post'])
			$pages[$pg->ID] = $pg->post_title." #$pg->ID";
        $fields = [
            Field::make( 'separator', 'sep_main', __('Kloking', 'TADS_L')),
			Field::make( 'checkbox', 'arista_kloker', __('Aktifkan Kloking', 'arista'))
				->set_default_value(false),
			Field::make( 'select', 'arista_kloker_type', __('Tipe Kloking', 'arista'))
				->set_width(100)
				->set_options([
					'own' => __('Tampilin Halaman Sendiri','arista'),
					'scrape' => __('Scrape URL','arista'),
					'redirect' => __('Redirect','arista'),
				]),
			Field::make( 'select', 'arista_kloker_page', __('Halaman Kloking', 'arista') )
				->set_options($pages)
				->set_conditional_logic([
					[
						'field' => 'arista_kloker_type',
						'value' => 'own',
					]
				]),
				
			Field::make( 'text', 'arista_kloker_url_redirect', __('URL Redirect', 'arista') )
				->set_conditional_logic([
					[
						'field' => 'arista_kloker_type',
						'value' => 'redirect',
					]
				]),
			Field::make( 'text', 'arista_kloker_url', __('URL yang di scrape', 'arista') )
				->set_conditional_logic([
					[
						'field' => 'arista_kloker_type',
						'value' => 'scrape',
					]
				]),
			Field::make( 'complex', 'arista_kloker_fr', 'Find & Replace')
				->set_layout('tabbed-vertical')
                ->add_fields( array(
                    Field::make( 'text', 'find' ),
                    Field::make( 'text', 'replace' ),
                ))
				->set_conditional_logic([
					[
						'field' => 'arista_kloker_type',
						'value' => 'scrape',
					]
				]),

			Field::make( 'select', 'arista_kloker_device', __('Kloking Device', 'arista'))
				->set_width(100)
				->set_options([
					'all' => __('Semua Device','arista'),
					'desktop' => __('Hanya Desktop','arista'),
					'mobile' => __('Hanya Mobile','arista'),
					'android' => __('Hanya Android','arista'),
					'iphone' => __('Hanya Iphone','arista'),
				]),
        ];

        $this->container->add_fields($fields);
    }
}