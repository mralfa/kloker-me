<?php
namespace TADS\App;

use Kosan\Framework\Module\OptionPageCarbon;
use Carbon_Fields\Field;

defined('ABSPATH') or die('Direct access not allowed');

class Admin extends OptionPageCarbon
{
    protected $name = "TADS";
    protected $label = "Arista Kloker";
    protected $title = "Arista Kloker";
    
    public function view()
    {
        $lic = get_option('_arista_license');
        $check = $this->check_license($lic);
        $data = json_decode($check);
        $status = $data->message;
        $fields = [
            Field::make( 'separator', 'sep_main', __('Masukan License Key', 'TADS_L')),

            Field::make( 'text', 'arista_license', __('License Key', 'TADS_L'))
                ->set_width(50)
                ->set_attribute('type', 'password')
                ->set_required(true),
            Field::make( 'html', 'html_export_tiktok', __('', 'TADS_L'))
                ->set_html("Status License: <strong>$status</strong>"),
        ];

        $this->container->add_fields($fields);
    }
    
    public function save_hook() {
        $license = $_POST['carbon_fields_compact_input']['_arista_license'];
        $check = $this->check_license($license, true);
        $data = json_decode($check);
        if($data->success) {
            update_option('_arista_license_active', true);
        }
    }

    function check_license($license, $save = false)
    {
        $url = TADS_URL."/api/license";
        $response = wp_remote_post( $url, array(
            'method' => 'POST',
            'body' => array( 'license' => $license, 'save' => $save ),
        ));
        
        if ( is_wp_error( $response ) ) {
           $error_message = $response->get_error_message();
           return "Something went wrong: $error_message";
        } else {
           $data = $response['body'];
           return $data;
        }
    }
}